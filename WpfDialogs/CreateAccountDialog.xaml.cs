﻿using System.Windows;
using System.Windows.Input;

namespace WpfDialogs
{
    /// <summary>
    /// Логика взаимодействия для InputDialog.xaml
    /// </summary>
    public partial class CreateAccountDialog : Window
    {
        public int AccountType { get; set; }
        public CreateAccountDialog(string title = "Ввод значения")
        {
            InitializeComponent();
            Group.Header = title;
        }

        private void Accept(object sender, RoutedEventArgs e)
        {
            AccountType = AccountTypeComboBox.SelectedIndex;
            this.DialogResult = true;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {

        }
    }
}
