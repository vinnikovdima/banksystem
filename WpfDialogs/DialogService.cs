﻿using Microsoft.Win32;
using System;

namespace WpfDialogs
{
    public class DialogService : IDialogService
    {
        public void ShowConfirmDialog(string message, Action confirm, Action cancel = null)
        {
            if (ConfirmDialog.ShowDialog(message) == true)
            {
                confirm?.Invoke();
            }
            else
            {
                cancel?.Invoke();
            }
        }

        public void ShowMessage(string message)
        {
            Message.Show(message);
        }

        public void ShowCreateAccountDialog(Action<int> confirm)
        {
            CreateAccountDialog createAccountDialog = new CreateAccountDialog();
            if (createAccountDialog.ShowDialog() == true)
            {
                confirm?.Invoke(createAccountDialog.AccountType);
            }
        }

        public void ShowExportDialog(Action<string> confirm)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Json file (*.json)|*.json|Xml file (*.xml)|*.xml";
            if (saveFileDialog.ShowDialog() == true)
            {
                confirm?.Invoke(saveFileDialog.FileName);
            }
        }

        public void ShowInputDialog(string message, Action<string> confirm)
        {
            InputDialog inputDialog = new InputDialog(message);

            if (inputDialog.ShowDialog() == true)
            {
                confirm?.Invoke(inputDialog.Parameter);
            }
        }

        public void ShowTransferDialog(Action<int, decimal> confirm)
        {
            TransferDialog transferDialog = new TransferDialog();

            if (transferDialog.ShowDialog() == true)
            {
                confirm?.Invoke(transferDialog.AccountNumber, transferDialog.Sum);
            }
        }
    }
}
