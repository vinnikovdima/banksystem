﻿using System.Windows;
using System.Windows.Input;

namespace WpfDialogs
{
    /// <summary>
    /// Логика взаимодействия для TransferDialog.xaml
    /// </summary>
    public partial class TransferDialog : Window
    {
        public TransferDialog(string title = "Перевод денежных средств")
        {
            InitializeComponent();
            Group.Header = title;
            InputAccountNumber.Focus();
        }

        public int AccountNumber { get; set; }
        public decimal Sum { get; set; }

        private void Accept(object sender, RoutedEventArgs e)
        {
            if(int.TryParse(InputAccountNumber.Text, out int accountNumber)
                && decimal.TryParse(InputSum.Text, out decimal sum))
            {
                AccountNumber = accountNumber;
                Sum = sum;
                this.DialogResult = true;
            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {

        }
    }
}
