﻿using System;

namespace WpfDialogs
{
    public interface IDialogService
    {
        void ShowConfirmDialog(string message, Action confirm, Action cancel = null);
        void ShowMessage(string message);
        void ShowCreateAccountDialog(Action<int> confirm);
        void ShowExportDialog(Action<string> confirm);
        void ShowInputDialog(string message, Action<string> confirm);
        void ShowTransferDialog(Action<int, decimal> confirm);
    }
}
