﻿using System.Windows;
using System.Windows.Input;

namespace WpfDialogs
{
    /// <summary>
    /// Логика взаимодействия для InputDialog.xaml
    /// </summary>
    public partial class ConfirmDialog : Window
    {
        public ConfirmDialog(string message, string title)
        {
            InitializeComponent();
            Group.Header = title;
            MessageTextBlock.Text = message;
        }

        public static bool? ShowDialog(string message, string title = "")
        {
            return new ConfirmDialog(message, title).ShowDialog();
        }

        private void Accept(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {

        }
    }
}
