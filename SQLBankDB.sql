﻿CREATE TABLE [dbo].[Customers] (
    [id]             INT           IDENTITY (1, 1) NOT NULL,
    [FirstName]      NVARCHAR (50) NOT NULL DEFAULT 'Имя',
    [LastName]       NVARCHAR (50) NOT NULL DEFAULT 'Фамилия',
    [Birthday]       DATE          NOT NULL DEFAULT '1991-08-25',
    [Address]        NVARCHAR (50) NOT NULL DEFAULT '-',
    [Passport]       NVARCHAR (50) NOT NULL DEFAULT '0000 000000',
    [Phone]          NVARCHAR (50) NOT NULL DEFAULT '+7 (000) 000-00-00',
    [BasePercentage] INT           NOT NULL,
    [Type]           NVARCHAR (50) NOT NULL
);