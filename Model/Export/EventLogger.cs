using Model.Export;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace BankSystem.Model
{
    public interface ILogger
    {
        IList<OperationEvent> Events { get; }
        void AddEvent(string Message);
        void Export(string filePath);
    }

    public class OperationEvent
    {
        public DateTime Time { get; set; }
        public string Message { get; set; }

        public OperationEvent() { }

        public OperationEvent(string message)
        {
            Time = DateTime.UtcNow;
            Message = message;
        }
    }

    public class EventLogger : ILogger
    {
        public IList<OperationEvent> Events { get; }

        public EventLogger()
        {
            Events = new ObservableCollection<OperationEvent>();
        }
            
        public void AddEvent(string message)
        {
            Events.Add(new OperationEvent(message));
        }

        public void Export(string filePath)
        {
            var exporter = ExporterFactory.GetExporter(Path.GetExtension(filePath));
            exporter.Export(filePath, Events.ToList());
        }
    }
}