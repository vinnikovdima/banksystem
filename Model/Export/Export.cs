﻿using Newtonsoft.Json;
using System.IO;
using System.Xml.Serialization;

namespace Model.Export
{
    public interface IExport
    {
        void Export<T>(string path, T exportData);
    }

    public static class ExporterFactory
    {
        public static IExport GetExporter(string fileExtension)
        {
            switch (fileExtension)
            {
                case ".json": return new JsonExporter();
                case ".xml": return new XmlExporter();
                default: return new NullExporter();
            }
        }
    }

    public class NullExporter : IExport
    {
        public void Export<T>(string path, T exportData) { }
    }

    public class JsonExporter : IExport
    {
        public void Export<T>(string path, T exportData)
        {
            string json = JsonConvert.SerializeObject(exportData, Formatting.Indented);
            File.WriteAllText(path, json);
        }
    }

    public class XmlExporter : IExport
    {
        public void Export<T>(string path, T exportData)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));

            using (var fStream = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                xmlSerializer.Serialize(fStream, exportData);
            }
        }
    }
}
