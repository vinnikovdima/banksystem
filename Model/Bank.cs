using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.Model
{
    public enum AccountType
    {
        WithСapitalized,
        WithoutСapitalized
    }

    public class Bank
    {
        public ILogger EventLogger { get; }
        
        public CustomerControll CustomerControll { get; set; }
        
        public AccountControll AccountControll { get; set; }

        public DepartmentControll DepartmentControll { get; set; }

        public Bank()
        {
            EventLogger = new EventLogger();

            AccountControll = new AccountControll(EventLogger);
            CustomerControll = new CustomerControll(AccountControll, EventLogger);
            DepartmentControll = new DepartmentControll(AccountControll, EventLogger);

            Load();
        }

        private void Load()
        {
            List<Task> loader = new List<Task>();

            using (var contextDB = new BankDBEntities())
            {
                var dBdepartments = contextDB.Departments.ToList();
                if (dBdepartments.Count == 0)
                    DepartmentControll.CreateDepartments();

                foreach (var dBdepartment in dBdepartments)
                {
                    var type = Type.GetType(dBdepartment.Type);
                    Type genericType = typeof(Department<>).MakeGenericType(type);

                    var department = (IDepartment<Customer>)Activator.CreateInstance(genericType, dBdepartment.Name);
                    department.Id = dBdepartment.Id;

                    DepartmentControll.Departments.Add(department);

                    loader.Add(Task.Factory.StartNew(
                        () => department.Load()));
                }

                loader.Add(Task.Factory.StartNew(
                   () =>
                   {
                       var accounts = contextDB.Accounts;
                       foreach (var account in accounts)
                       {
                           var type = Type.GetType(account.Type);

                           var convertedAccount = (BlockableAccount)Activator.CreateInstance(type);
                           convertedAccount.FillFromAccounts(account);
                           AccountControll.AddAccount(convertedAccount);
                       }
                   }));

                Task.WaitAll(loader.ToArray());
            }

            var customers = DepartmentControll.Departments.SelectMany(x => x.Customers);

            foreach (var account in AccountControll.Accounts)
            {
                var customer = customers.FirstOrDefault(c => c.Id == account.CustomerId);

                if (customer != null)
                    account.Customer = customer;
            }
        }
    }
}