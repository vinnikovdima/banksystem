using Model;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BankSystem.Model
{
    public class Account : Accounts, INotifyPropertyChanged, IAccount
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [JsonIgnore]
        public Customer Customer { get; set; }

        public virtual decimal CalcSum => TotalSum;

        public decimal TotalSum
        {
            get => Sum;
            set
            {
                if (value == Sum) return;
                Sum = value;
                OnPropertyChanged(nameof(CalcSum));
            }
        }

        public Account() : this(null)
        {

        }

        public Account(Customer customer)
        {
            if(customer != null)
                CustomerId = customer.Id;
            Customer = customer;
            Type = "������� ����";
            CreateDate = DateTime.UtcNow;
        }

        public void FillFromAccounts(Accounts baseType)
        {
            Id = baseType.Id;
            Sum = baseType.Sum;
            CustomerId = baseType.CustomerId;
            CreateDate = baseType.CreateDate;
        }

        public virtual void Put(decimal sum)
        {
            TotalSum += sum;
        }

        public virtual decimal Withdraw(decimal sum)
        {
            if (TotalSum < sum) throw new InsufficientFundsException();
            
            TotalSum -= sum;
            return sum;
        }

        public virtual void Calculate(DateTime date) { }

        public static bool operator >(Account account, Account otherAccount)
            => account.TotalSum > otherAccount.TotalSum;
        public static bool operator <(Account account, Account otherAccount)
            => account.TotalSum < otherAccount.TotalSum;

        public void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public override string ToString()
        {
            return $"{Type} �{Id}";
        }
    }
}