﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem.Model
{
    public class СapitalizedСontribution : BlockableAccount
    {
        public override decimal CalcSum => TotalSum * PercentageSum;

        private decimal PercentageSum = 1;

        public СapitalizedСontribution() : this(null) {
        }

        public СapitalizedСontribution(Customer customer) : base(customer)
        {
            Type = "Вклад с капитализацией";
        }

        public override void Calculate(DateTime date)
        {
            if (date.CompareTo(CreateDate) > 0)
            {
                int yearCount = (int)date.Subtract(CreateDate).TotalDays / 365;
                
                PercentageSum = (decimal) Math.Pow(1 + (Customer.Percentage / 100.0) / 12.0, 12 * yearCount);

                OnPropertyChanged(nameof(CalcSum));
            }
        }
    }
}
