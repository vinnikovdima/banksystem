﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace BankSystem.Model
{
    public interface IBlockable
    {
        bool HasAccess { get; }
        TimeSpan TimeBlocking { get; }
    }

    public class BlockableAccount : Account, IBlockable
    {
        private readonly TimeSpan MaxDeltaMinutes = TimeSpan.FromMinutes(5);
        private readonly TimeSpan TimeoutSecconds = TimeSpan.FromMinutes(1);
        private readonly int MaxTransactionCount = 3;

        private DateTime _lastTimeTransaction = DateTime.UtcNow;
        private int _transactionCount;

        DispatcherTimer _timer;

        public BlockableAccount() : base() { }

        public BlockableAccount(Customer customer) : base(customer) { }

        private async Task BlockingAsync()
        {
            bool isBlocking = true;
            while(isBlocking)
            {
                var timeDelta = DateTime.UtcNow.Subtract(_lastTimeTransaction);
                
                TimeBlocking = TimeoutSecconds - timeDelta;
                
                if (timeDelta >= TimeoutSecconds)
                {
                    TimeBlocking = TimeSpan.Zero;
                    _transactionCount = 0;
                    isBlocking = false;
                }

                await Task.Delay(1000);
            }
        }

        private async void StartBlockingAsync()
        {
            await Task.Run(() => BlockingAsync());
            HasAccess = true;
        }

        private bool _hasAccess = true;
        public bool HasAccess
        {
            get => _hasAccess;
            set
            {
                if (value == _hasAccess) return;
                _hasAccess = value;

                OnPropertyChanged(nameof(HasAccess));

                if (!_hasAccess)
                {
                    StartBlockingAsync();
                }
            }
        }

        TimeSpan _timeBlocking;
        public TimeSpan TimeBlocking
        {
            get => _timeBlocking;
            set
            {
                if (value == _timeBlocking) return;
                _timeBlocking = value;

                OnPropertyChanged(nameof(TimeBlocking));
            }
        }

        private bool CheckTransaction()
        {
            if (DateTime.UtcNow.Subtract(_lastTimeTransaction) < MaxDeltaMinutes)
            {
                _transactionCount++;
                if (_transactionCount >= MaxTransactionCount + 1)
                {
                    return false;
                }
            }
            else
            {
                if (_transactionCount < MaxTransactionCount + 1)
                    _transactionCount = 0;
            }

            _lastTimeTransaction = DateTime.UtcNow;

            return true;
        }

        public override void Put(decimal sum)
        {
            HasAccess = CheckTransaction();
            if (!HasAccess)
                throw new AccessOperationException();

            base.Put(sum);
        }

        public override decimal Withdraw(decimal sum)
        {
            HasAccess = CheckTransaction();
            if (!HasAccess)
                throw new AccessOperationException();

            var result = base.Withdraw(sum);

            return result;
        }
    }
}
