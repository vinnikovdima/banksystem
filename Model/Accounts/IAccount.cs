using System;

namespace BankSystem.Model
{
    public interface IAccount
    {
        int Id { get; set; }
        int CustomerId { get; }
        Customer Customer { get; set; }
        DateTime CreateDate { get; set; }
        decimal TotalSum { get; }
        void Put(decimal sum);
        decimal Withdraw(decimal sum);
        void Calculate(DateTime date);
    }
}