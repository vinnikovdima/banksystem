﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem.Model
{
    public class AccountComparer<T> : IComparer<T> where T : Account
    {
        public int Compare(T first, T second)
        {
            if (first > second)
                return 1;
            else if (first < second)
                return -1;
            else
                return 0;
        }
    }

    public class AccountDescendingComparer<T> : IComparer<T> where T : Account
    {
        public int Compare(T first, T second)
        {
            if (first < second)
                return 1;
            else if (first > second)
                return -1;
            else
                return 0;
        }
    }
}
