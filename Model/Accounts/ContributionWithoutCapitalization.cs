﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem.Model
{
    public class ContributionWithoutCapitalization : BlockableAccount
    {
        public override decimal CalcSum => TotalSum + PercentageSum;

        private decimal PercentageSum;

        public ContributionWithoutCapitalization() : this(null) { }

        public ContributionWithoutCapitalization(Customer customer) : base(customer)
        {
            Type = "Вклад без капитализацией";
        }

        public override void Calculate(DateTime date)
        {
            if(date.CompareTo(CreateDate) > 0)
            {
                int yearCount = (int)date.Subtract(CreateDate).TotalDays / 365;
                decimal percentage = Customer.Percentage / 100.0m;
                PercentageSum = TotalSum * percentage * yearCount;
                OnPropertyChanged(nameof(CalcSum));
            }
        }
    }
}
