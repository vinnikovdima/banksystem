﻿CREATE TABLE [dbo].[Customers] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [DepartmentId]   INT           NOT NULL,
    [FirstName]      NVARCHAR (50) DEFAULT ('FirstName') NOT NULL,
    [LastName]       NVARCHAR (50) DEFAULT ('LastName') NOT NULL,
    [Birthday]       DATETIME      DEFAULT ('1991-08-25') NOT NULL,
    [Address]        NVARCHAR (50) DEFAULT ('-') NOT NULL,
    [Passport]       NVARCHAR (50) DEFAULT ('0000 000000') NOT NULL,
    [Phone]          NVARCHAR (50) DEFAULT ('+7 (000) 000-00-00') NOT NULL,
    [BasePercentage] INT           NOT NULL,
    CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[Accounts] (
    [Id]         INT           IDENTITY (1, 1) NOT NULL,
    [CustomerId] INT           NOT NULL,
    [Type]       NVARCHAR (50) NOT NULL,
    [CreateDate] DATETIME      NOT NULL,
    [Sum]        DECIMAL (18)  NOT NULL,
    CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[Departments] (
    [Id]   INT           IDENTITY (1, 1) NOT NULL,
    [Name] NVARCHAR (50) NOT NULL,
    [Type] NVARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

select * from Customers

select * from Accounts

select * from Departments

delete from Customers where id = 1

delete from Accounts where id = 1

delete from Departments where id = 1

delete from Accounts

delete from Customers

delete from Departments

select * from Customers, Departments where Customers.DepartmentId = Departments.id and Customers.DepartmentId = 13