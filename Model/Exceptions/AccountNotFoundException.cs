﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem.Model
{
    public class AccountNotFoundException : Exception
    {
        public int AccountId { get; }

        public AccountNotFoundException(int accountId)
        {
            AccountId = accountId;
        }
    }
}
