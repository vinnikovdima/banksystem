using System;

namespace BankSystem.Model
{
    public class CorporateClient : Customer
    {
        public CorporateClient()
        {
            BasePercentage = 15;
            Type = nameof(CorporateClient);
        }
        
        public override string ToString()
        {
            return "����������� ����";
        }
    }
}