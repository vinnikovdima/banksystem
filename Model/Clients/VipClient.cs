using System;

namespace BankSystem.Model
{
    public class VipClient : Customer
    {
        public VipClient()
        {
            BasePercentage = 12;
            Type = nameof(VipClient);
        }

        public override string ToString()
        {
            return "VIP ������";
        }
    }
}