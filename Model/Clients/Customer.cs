using Model;
using Newtonsoft.Json;
using System;

namespace BankSystem.Model
{
    public abstract class Customer : Customers
    {
        public virtual int Percentage => BasePercentage;
        public string Type { get; set; }

        public Customer()
            : this("Имя", "Фамилия", DateTime.Today, "-", "0000 000000", "+7 (000) 000-00-00")
        {
            
        }
        
        public Customer(string firstName, string lastName, DateTime birthday, string address, string passport, string phone)
        {
            //Id = ++_counter;
            FirstName = firstName;
            LastName = lastName;
            Birthday = birthday;
            Address = address;
            Passport = passport;
            Phone = phone;
        }
    }
}