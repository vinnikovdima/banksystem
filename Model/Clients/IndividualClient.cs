using System;

namespace BankSystem.Model
{
    public class IndividualClient : Customer
    {
        public IndividualClient()
        {
            Type = nameof(IndividualClient);
            BasePercentage = 5;
        }

        public override string ToString()
        {
            return "������� ����";
        }
    }
}