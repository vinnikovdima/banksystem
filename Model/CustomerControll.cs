﻿using BankSystem.Helpers;

namespace BankSystem.Model
{
    public class CustomerControll : ObservableObject
    {
        private AccountControll _accountControll;

        private Customer _customer;
        private ILogger _eventLogger;

        public CustomerControll(AccountControll accountControll, ILogger eventLogger)
        {
            _accountControll = accountControll;
            _eventLogger = eventLogger;
        }

        public Customer Customer 
        {
            get => _customer;
            set
            {
                if(_customer != value)
                {
                    _customer = value;
                    OnPropertyChanged();
                }
            }
        }

        public void CreateIndividualAccount(int accountType)
        {
            if (_customer == null) return;

            BlockableAccount account;

            if (accountType == (int)AccountType.WithСapitalized)
            {
                account = new СapitalizedСontribution(_customer);
            }
            else
            {
                account = new ContributionWithoutCapitalization(_customer);
            }

            _accountControll.CreateAccount(account);

            _eventLogger.AddEvent(
                   $"Открыт {account} под {account.Customer.Percentage} % для {account.Customer.FullName()}");
        }
    }
}