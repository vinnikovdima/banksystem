﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem.Model
{
    public static class Extensions
    {
        public static string FullName(this Customer customer)
            => $"{customer.FirstName} {customer.LastName}";
    }
}
