﻿using Model;
using System.Collections.Generic;

namespace BankSystem.Model
{
    public class DepartmentControll
    {
        public IList<IDepartment<Customer>> Departments { get; private set; }
        private AccountControll _accountControll;
        private ILogger _eventLogger;

        public DepartmentControll(AccountControll accountControll, ILogger eventLogger)
        {
            _accountControll = accountControll;
            _eventLogger = eventLogger;
            Departments = new List<IDepartment<Customer>>();
        }

        
        public int ActiveDepartment { get; set; }

        public void CreateDepartments()
        {
            using (var contextDB = new BankDBEntities())
            {
                contextDB.Departments.Add(CreateDepartment<IndividualClient>("Отдел работы с обычными клиентами"));
                contextDB.Departments.Add(CreateDepartment<VipClient>("Отдел работы с VIP клиентами"));
                contextDB.Departments.Add(CreateDepartment<CorporateClient>("Отдел работы с юридическими лицами"));
                contextDB.SaveChanges();
            }
        }

        private Departments CreateDepartment<T>(string name) where T : Customer, new()
        {
            var department = new Departments()
            {
                Name = name,
                Type = typeof(T).FullName
            };
            
            return department;
        }

        public void RemoveCustomer(Customer customer)
        {
            if (Departments.Count > ActiveDepartment)
            {
                Departments[ActiveDepartment].RemoveCustomerById(customer.Id);

                foreach (BlockableAccount account in _accountControll.GetAccounts(customer))
                {
                    _accountControll.RemoveAccount(account);
                    _eventLogger.AddEvent($"Удален клиент из {Departments[ActiveDepartment].Name}");
                }
            }
        }

        public void UpdateCustomer(Customer customer)
        {
            if (Departments.Count > ActiveDepartment)
            {
                Departments[ActiveDepartment].UpdateCustomer(customer.Id);
            }
        }

        public void CreateCustomer()
        {
            if (Departments.Count > ActiveDepartment)
            {
                var customer = Departments[ActiveDepartment].CreateCustomer();
                _accountControll.CreateAccount(new BlockableAccount(customer));
                _eventLogger.AddEvent($"Добавлен новый клиент в {Departments[ActiveDepartment].Name}");
            }
        }
    }
}