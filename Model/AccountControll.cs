﻿using Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;

namespace BankSystem.Model
{
    public class AccountControll
    {
        private ILogger _eventLogger;
        public IList<Account> Accounts { get; private set; }

        public AccountControll(ILogger eventLogger)
        {
            _eventLogger = eventLogger;
            Accounts = new ObservableCollection<Account>();
        }

        public void AddAccount(BlockableAccount account)
        {
            account.PropertyChanged += BlockableAccount_PropertyChanged;
            Accounts.Add(account);
        }

        public void CreateAccount(BlockableAccount account)
        {
            var accountDb = new Accounts()
            {
                CustomerId = account.CustomerId,
                CreateDate = account.CreateDate,
                Sum = account.TotalSum,
                Type = account.GetType().FullName
            };

            using (var contextDB = new BankDBEntities())
            {
                contextDB.Accounts.Add(accountDb);
                contextDB.SaveChanges();
            }

            account.Id = accountDb.Id;

            AddAccount(account);
        }

        public List<BlockableAccount> GetAccounts(Customer customer)
        {
            return Accounts.Where(x => x.Customer == customer)
                           .Cast<BlockableAccount>()
                           .ToList();
        }
        public void RemoveAccount(BlockableAccount account)
        {
            account.PropertyChanged -= BlockableAccount_PropertyChanged;

            using (var contextDB = new BankDBEntities())
            {
                var accountDb = contextDB.Accounts.Find(account.Id);
                if (accountDb != null)
                {
                    contextDB.Accounts.Remove(accountDb);
                    contextDB.SaveChanges();
                }
            }

            Accounts.Remove(account);
        }
        private void BlockableAccount_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var blockableAccount = sender as BlockableAccount;
            if (e.PropertyName == nameof(blockableAccount.HasAccess))
            {
                string state = blockableAccount.HasAccess ? "разблокирован" : "заблокирован";
                _eventLogger.AddEvent($"Cчет №{blockableAccount.Id} {state}");
            }
        }
        public void PutMoney(int accountId, decimal sum)
        {
            var account = Accounts.FirstOrDefault(a => a.Id == accountId);
            if (account == null) return;

            using (var contextDB = new BankDBEntities())
            {
                var accountDb = contextDB.Accounts.Find(accountId);
                if (accountDb != null)
                {
                    account.Put(sum);
                    accountDb.Sum = account.Sum;
                    contextDB.SaveChanges();
                    _eventLogger.AddEvent($"Пополнение счета №{accountId} на сумму {sum:C}");
                }
            }
        }
        public void WithdrawMoney(int accountId, decimal sum)
        {
            var account = Accounts.FirstOrDefault(a => a.Id == accountId);
            if (account == null) return;

            using (var contextDB = new BankDBEntities())
            {
                var accountDb = contextDB.Accounts.Find(accountId);
                if (accountDb != null)
                {
                    account.Withdraw(sum);
                    accountDb.Sum = account.Sum;
                    contextDB.SaveChanges();
                    _eventLogger.AddEvent($"Снятие со счета №{accountId} суммы {sum:C}");
                }
            }
        }
        public void TransferMoney(int accountId, int otherAccountId, decimal sum)
        {
            var account = Accounts.FirstOrDefault(a => a.Id == accountId);
            var other = Accounts.FirstOrDefault(a => a.Id == otherAccountId);

            using (var contextDB = new BankDBEntities())
            {
                var accountsDb = contextDB.Accounts.ToList();
                var accountDb = accountsDb.FirstOrDefault(a => a.Id == accountId);
                var otherAccountDb = accountsDb.FirstOrDefault(a => a.Id == otherAccountId);

                if (account == null || accountDb == null) throw new AccountNotFoundException(accountId);
                if (other == null || otherAccountDb == null) throw new AccountNotFoundException(otherAccountId);

                if (account is BlockableAccount blockableAccount && !blockableAccount.HasAccess)
                    throw new AccessOperationException();

                if (other is BlockableAccount otherBlockableAccount && !otherBlockableAccount.HasAccess)
                    throw new AccessOperationException();

                if (account.TotalSum < sum)
                    throw new InsufficientFundsException();

                other.Put(account.Withdraw(sum));

                accountDb.Sum = account.Sum;
                otherAccountDb.Sum = other.Sum;

                contextDB.SaveChanges();

                _eventLogger.AddEvent(
                        $"Перевод средств со счета №{accountId} на счет №{otherAccountId} суммы {sum:C}");
            }
        }
        public void CalculateAll(DateTime dateTime)
        {
            foreach (IAccount account in Accounts)
            {
                account.Calculate(dateTime);
            }
        }
        public void Calculate(int accountId, DateTime dateTime)
        {
            Accounts.FirstOrDefault(a => a.Id == accountId)?.Calculate(dateTime);
        }
    }
}