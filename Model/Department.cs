using Model;
using Model.Export;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace BankSystem.Model
{
    public interface IDepartment<out T>
    {
        int Id { get; set; }
        string Name { get; }
        IEnumerable<T> Customers { get; }
        T CreateCustomer();
        void RemoveCustomerById(int id);
        void UpdateCustomer(int id);
        void Load();
        void Export(string path);
    }
    
    public class Department<T> : IDepartment<T> where T : Customer, new()
    {
        public IEnumerable<T> Customers => _customers;
        private readonly IList<T> _customers;
        public int Id { get; set; }
        public string Name { get; }
        public string Type { get; }

        BankDBEntities bankDB;

        public Department(string name)
        {
            Type = nameof(T);
            Name = name;
            _customers = new ObservableCollection<T>();
            bankDB = new BankDBEntities();
        }
        
        public T CreateCustomer()
        {
            T customer = new T();

            var customerDb = new Customers() {
                DepartmentId = Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Birthday = customer.Birthday,
                Address = customer.Address,
                Passport = customer.Passport,
                Phone = customer.Phone,
                BasePercentage = customer.BasePercentage
            };

            bankDB.Customers.Add(customerDb);
            bankDB.SaveChanges();

            customer.Id = customerDb.Id;

            AddCustomer(customer);
            return customer;
        }

        public void RemoveCustomerById(int id)
        {
            var findedCustomer = bankDB.Customers.Find(id);
            if (findedCustomer != null)
            {
                bankDB.Customers.Remove(findedCustomer);
                bankDB.SaveChanges();
            }

            var obj= _customers.FirstOrDefault(x => x.Id == id);
            
            if (obj != null)
            {
                RemoveCustomer(obj);
            }
        }
        
        public void UpdateCustomer(int id)
        {
            var obj = _customers.FirstOrDefault(x => x.Id == id);
            if (obj != null)
            {
                var customer = bankDB.Customers.Find(id);
                if(customer != null)
                {
                    customer.FirstName = obj.FirstName;
                    customer.LastName = obj.LastName;
                    customer.Birthday = obj.Birthday;
                    customer.Address = obj.Address;
                    customer.Passport = obj.Passport;
                    customer.Phone = obj.Phone;
                    bankDB.SaveChanges();
                }
            }
        }

        public void Load()
        {
            var customers = bankDB.Customers.Where(x => x.DepartmentId == Id);
            foreach (var dBcustomer in customers)
            {
                var customer = new T()
                {
                    Id = dBcustomer.Id,
                    FirstName = dBcustomer.FirstName,
                    LastName = dBcustomer.LastName,
                    Birthday = dBcustomer.Birthday,
                    Address = dBcustomer.Address,
                    Passport = dBcustomer.Passport,
                    Phone = dBcustomer.Phone,
                    BasePercentage = dBcustomer.BasePercentage,
                };
                AddCustomer(customer);
            }
        }

        public void Export(string path)
        {
            var exporter = ExporterFactory.GetExporter(Path.GetExtension(path));
            exporter.Export(path, _customers.ToList());
        }

        private void AddCustomer(T customer)
        {
            _customers.Add(customer);
        }
        
        private void RemoveCustomer(T customer)
        {
            _customers.Remove(customer);
        }
    }
}