﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BankSystem.Views
{
    /// <summary>
    /// Логика взаимодействия для InputDialog.xaml
    /// </summary>
    public partial class InputDialog : Window
    {
        public InputDialog(string title = "Ввод значения")
        {
            InitializeComponent();
            Group.Header = title;
            Input.Focus();
        }

        public string Parameter { get; set; }

        private void Accept(object sender, RoutedEventArgs e)
        {
            Parameter = Input.Text;
            this.DialogResult = true;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {

        }
    }
}
