﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BankSystem.Views
{
    /// <summary>
    /// Логика взаимодействия для InputDialog.xaml
    /// </summary>
    public partial class ConfirmDialog : Window
    {
        public ConfirmDialog(string message, string title)
        {
            InitializeComponent();
            Group.Header = title;
            MessageTextBlock.Text = message;
        }

        public static bool? ShowDialog(string message, string title = "")
        {
            return new ConfirmDialog(message, title).ShowDialog();
        }

        private void Accept(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {

        }
    }
}
