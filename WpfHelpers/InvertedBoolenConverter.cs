﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace BankSystem.Helpers
{
    public class ValueConverterGroup : List<IValueConverter>, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo language)
        {
            object curValue;

            curValue = value;
            for (int i = 0; i < this.Count; i++)
            {
                curValue = this[i].Convert(curValue, targetType, parameter, language);
            }

            return (curValue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo language)
        {
            object curValue;

            curValue = value;
            for (int i = (this.Count - 1); i >= 0; i--)
            {
                curValue = this[i].ConvertBack(curValue, targetType, parameter, language);
            }

            return (curValue);
        }
    }

    public class InvertBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo language)
        {
            if (value is bool)
            {
                return (!(bool)value);
            }
            else
            {
                throw new ArgumentException("Value must be of the type bool");
            }
        }

        public object ConvertBack(object value,  Type targetType, object parameter, CultureInfo language)
        {
            if (value is bool)
            {
                return (!(bool)value);
            }
            else
            {
                throw new ArgumentException();
            }
        }
    }
   /* public class InvertedBoolenConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }
    }*/
}
