﻿using BankSystem.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfDialogs;

namespace BankSystem.ViewModels
{
    class AccountsViewModel
    {
        private Bank _bank;

        private IDialogService _dialogService;

        public AccountsViewModel(Bank bank, IDialogService dialogService)
        {
            _bank = bank;
            _dialogService = dialogService;
        }

        public IList<Account> Accounts => _bank.AccountControll.Accounts;
    }
}
