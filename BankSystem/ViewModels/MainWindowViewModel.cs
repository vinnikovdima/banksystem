﻿using BankSystem.Helpers;
using BankSystem.Model;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BankSystem.ViewModels
{
    class MainWindowViewModel : ObservableObject
    {
        private readonly PaletteHelper _paletteHelper = new PaletteHelper();
        private Bank _bank { get; }

        public MainWindowViewModel(Bank bank)
        {
            _bank = bank;
        }

        public DateTime DateOfCalculation { get; set; } = DateTime.UtcNow;
        public ICommand SelectDateCommand 
            => new RelayCommand(() => _bank.AccountControll.CalculateAll(DateOfCalculation));

        public ICommand ChangeThemeCommand 
            => new RelayCommand<bool>(isDark => ToggleBaseColour(isDark));

        private void ToggleBaseColour(bool isDark)
        {
            ITheme theme = _paletteHelper.GetTheme();

            if(isDark)
            {
                theme.SetBaseTheme(new MaterialDesignDarkTheme());
            }
            else
            {
                theme.SetBaseTheme(new MaterialDesignLightTheme());
            }

            _paletteHelper.SetTheme(theme);
        }

    }
}
