﻿using BankSystem.Helpers;
using BankSystem.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfDialogs;

namespace BankSystem.ViewModels
{
    public class AccountControlViewModel : ObservableObject
    {
        private Bank _bank;
        private IDialogService _dialogService;

        public AccountControlViewModel(Bank bank, IDialogService dialogService)
        {
            _bank = bank;
            _dialogService = dialogService;

            _bank.CustomerControll.PropertyChanged += (s, e)
                =>
                {
                    OnPropertyChanged(nameof(Accounts));
                    SelectedIndex = 0;
                };
        }

        private int _selectedIndex;
        public int SelectedIndex 
        {
            get => _selectedIndex;
            set
            {
                _selectedIndex = value;
                OnPropertyChanged();
            }
        }

        public IList<BlockableAccount> Accounts => _bank.AccountControll.GetAccounts(_bank.CustomerControll.Customer);

        public ICommand PutCommand
          => new RelayCommand<int>((id) =>
          {
              _dialogService.ShowInputDialog("Пополнение счета", inputValue => 
                   {
                       if (decimal.TryParse(inputValue, out decimal sum))
                       {
                           try
                           {
                               _bank.AccountControll.PutMoney(id, sum);
                           }
                           catch (AccessOperationException)
                           {
                               _dialogService.ShowMessage("Операция заблокирована");
                           }
                       }
                   });
          });

        public ICommand WithdrawCommand
         => new RelayCommand<int>((id) =>
         {
             _dialogService.ShowInputDialog("Снятие средств со счета", inputValue =>
             {
                 if (decimal.TryParse(inputValue, out decimal sum))
                 {
                     try
                     {
                         _bank.AccountControll.WithdrawMoney(id, sum);
                     }
                     catch (AccessOperationException)
                     {
                         _dialogService.ShowMessage("Операция заблокирована");
                     }
                     catch (InsufficientFundsException)
                     {
                         _dialogService.ShowMessage("Недостаточно средств");
                     }
                 }
             });
         });

        public ICommand TransferCommand
         => new RelayCommand<int>((id) =>
         {
             _dialogService.ShowTransferDialog((remoteId, sum) =>
             {
                 try
                 {
                     _bank.AccountControll.TransferMoney(id, remoteId, sum);
                 }
                 catch (AccountNotFoundException ex)
                 {
                     Message.Show($"Счет {ex.AccountId} не найден");
                 }
                 catch (AccessOperationException)
                 {
                     Message.Show("Операция заблокирована");
                 }
                 catch (InsufficientFundsException)
                 {
                     Message.Show("Недостаточно средств");
                 }
             });
         });
    }
}
