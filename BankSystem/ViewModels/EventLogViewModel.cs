﻿using BankSystem.Helpers;
using BankSystem.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfDialogs;

namespace BankSystem.ViewModels
{
    public class EventLogViewModel
    {
        private ILogger _eventLogger;
        private IDialogService _dialogService;

        public EventLogViewModel(ILogger eventLogger, IDialogService dialogService)
        {
            _eventLogger = eventLogger;
            _dialogService = dialogService;
        }

        public IList<OperationEvent> Events => _eventLogger.Events;

        public ICommand ExportCommand
           => new RelayCommand(() =>
           {
               _dialogService.ShowExportDialog(
                   filePath => _eventLogger.Export(filePath));
           });
    }
}
