﻿using BankSystem.Helpers;
using BankSystem.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfDialogs;

namespace BankSystem.ViewModels
{
    class UserInfoViewModel : ObservableObject
    {
        private Bank _bank;

        private IDialogService _dialogService;

        public UserInfoViewModel(Bank bank, IDialogService dialogService)
        {
            _bank = bank;
            _dialogService = dialogService;

            _bank.CustomerControll.PropertyChanged += (s, e)
                => OnPropertyChanged(nameof(CustomerInfo));
        }

        public Customer CustomerInfo => _bank.CustomerControll.Customer;
        
        public ICommand CreateAccountCommand
           => new RelayCommand(() =>
           {
                _dialogService.ShowCreateAccountDialog(
                    accountType => _bank.CustomerControll.CreateIndividualAccount(accountType));
           });

        public ICommand CustomerUpdateCommand
            => new RelayCommand(() => {
                if (_bank.CustomerControll.Customer != null)
                    _bank.DepartmentControll.UpdateCustomer(_bank.CustomerControll.Customer);
                });
    }
}
