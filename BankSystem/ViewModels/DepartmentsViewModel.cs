﻿using BankSystem.Helpers;
using BankSystem.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfDialogs;

namespace BankSystem.ViewModels
{
    class DepartmentsViewModel
    {
        private Bank _bank;

        private IDialogService _dialogService;

        public DepartmentsViewModel(Bank bank, IDialogService dialogService)
        {
            _bank = bank;
            _dialogService = dialogService;
        }
        
        public IList<IDepartment<Customer>> Departments => _bank.DepartmentControll.Departments;

        public int SelectedDepartment
        {
            get => _bank.DepartmentControll.ActiveDepartment;
            set => _bank.DepartmentControll.ActiveDepartment = value;
        }

        public Customer SelectedCustomer
        {
            get => _bank.CustomerControll.Customer;
            set => _bank.CustomerControll.Customer = value;
        }

        public ICommand AddCustomerCommand
           => new RelayCommand(() => _bank.DepartmentControll.CreateCustomer());

        public ICommand RemoveCustomerCommand
          => new RelayCommand<Customer>(customer =>
          {
              _dialogService.ShowConfirmDialog("Удалить клиента?",
                  () => _bank.DepartmentControll.RemoveCustomer(customer));
          });
    }
}
