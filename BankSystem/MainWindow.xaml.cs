﻿using BankSystem.Helpers;
using BankSystem.Model;
using BankSystem.ViewModels;
using BankSystem.Views;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using WpfDialogs;

namespace BankSystem
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            IDialogService dialogService = new DialogService();
            Bank bank = new Bank();

            DataContext = new MainWindowViewModel(bank);
            Departments.DataContext = new DepartmentsViewModel(bank, dialogService);
            AccauntsList.DataContext = new AccountsViewModel(bank, dialogService);
            UserInfo.DataContext = new UserInfoViewModel(bank, dialogService);
            EventLogList.DataContext = new EventLogViewModel(bank.EventLogger, dialogService);
            AccountControl.DataContext = new AccountControlViewModel(bank, dialogService);
            // Departments.ListViewClients.SelectionChanged += ListViewClients_SelectionChanged;

            /* 
             AccountControl.PutMoney =
                 (id, sum) =>
                 {
                     Bank.PutMoney(id, sum);
                     Bank.Calculate(id, TimeMachine.SelectedDate ?? DateTime.Now);
                 };

             AccountControl.WithdrawMoney =
                 (id, sum) =>
                 {
                     Bank.WithdrawMoney(id, sum);
                     Bank.Calculate(id, TimeMachine.SelectedDate ?? DateTime.Now);
                 };

             AccountControl.TransferMoney =
                (id, other_id, sum) =>
                {
                    Bank.TransferMoney(id, other_id, sum);
                    Bank.Calculate(id, TimeMachine.SelectedDate ?? DateTime.Now);
                };
            */
        }



        /* private void ListViewClients_SelectionChanged(object sender, SelectionChangedEventArgs e)
         {
             if (e.AddedItems.Count > 0)
             {
                 UpdateCustomerAccounts((Customer)e.AddedItems[0]);
             }
         }

         private void UpdateCustomerAccounts(Customer customer)
         {
             List<BlockableAccount> accounts = Bank.GetAccounts(customer);
             accounts.Sort(new AccountDescendingComparer<BlockableAccount>());
             AccountControl.SetAccaunts(accounts);
         }
         */

        private void MainWindow_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
    }
}
