﻿using BankSystem.Model;
using System;
using System.Windows.Controls;

namespace BankSystem.Views
{
    /// <summary>
    /// Логика взаимодействия для Departments.xaml
    /// </summary>
    public partial class Departments : UserControl
    {
        public ListView CustomersListView => ListViewClients;

        public Departments()
        {
            InitializeComponent();
        }
    }
}