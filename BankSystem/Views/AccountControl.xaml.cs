﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using BankSystem.Model;

namespace BankSystem.Views
{
    /// <summary>
    /// Логика взаимодействия для AccountControl.xaml
    /// </summary>
    public partial class AccountControl : UserControl
    {
        public int AccountId { get; set; }

        public AccountControl()
        {
            InitializeComponent();
        }
    }
}
